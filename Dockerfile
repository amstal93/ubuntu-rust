# ubuntu-rust

ARG DOCKER_REGISTRY_URL=registry.gitlab.com/docking/
ARG CUSTOM_VERSION=1.0
ARG UBUNTU_VERSION=18.04
ARG RUST_VERSION=1.51.0

ARG KCOV_VERSION=38


FROM ${DOCKER_REGISTRY_URL}ubuntu-rust-mini:${CUSTOM_VERSION}-${UBUNTU_VERSION}-${RUST_VERSION} AS ubuntu-rust

ARG KCOV_VERSION

WORKDIR /root

RUN \
	apt-get -q -y update ; \
	apt-get -q -y install \
		binutils-dev \
		build-essential \
		clang \
		cmake \
		curl \
		debsigs \
		git \
		jq \
		libclang-dev \
		libdw-dev \
		libiberty-dev \
		libjemalloc-dev \
		libpcap-dev \
		libssl-dev \
		llvm \
		openssl \
		pkg-config \
		python-minimal \
		python3-minimal \
		unzip \
		zip \
		zlib1g-dev \
	; \
	apt-get -q -y clean ; \
	find /var/lib/apt/lists/ -type f -delete

RUN \
	export CARGO_HOME="/usr/local" ; \
	rustup component add \
		clippy \
		rustfmt \
	; \
	cargo install \
		cargo-cache \
		cargo-deb \
	; \
	cargo install --force --git https://github.com/romac/cargo-build-deps ; \
	cargo cache -r all

RUN \
	curl -Lo kcov-${KCOV_VERSION}.tar.gz https://github.com/SimonKagstrom/kcov/archive/v${KCOV_VERSION}.tar.gz && ( \
		tar zxf kcov-${KCOV_VERSION}.tar.gz ; \
		mkdir -p kcov-${KCOV_VERSION}/build ; \
		cd kcov-${KCOV_VERSION}/build ; \
		cmake .. ; \
		make -j $(nproc) ; \
		make install ; \
	) ; \
	rm -fR kcov-${KCOV_VERSION} kcov-${KCOV_VERSION}.tar.gz
